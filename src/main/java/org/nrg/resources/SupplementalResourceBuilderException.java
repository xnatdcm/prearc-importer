package org.nrg.resources;

public class SupplementalResourceBuilderException extends Exception {
    public SupplementalResourceBuilderException(String message) {
        super(message);
    }
}
